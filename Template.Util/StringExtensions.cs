﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template.Util
{
    public static class StringExtensions
    {
        public static bool HasValue(this string value)
        {
            if (value == null) return false;

            return !string.IsNullOrEmpty(value.Trim());
        }

        public static bool IsValidEmail(this string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
