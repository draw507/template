﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template.Util
{
    public static class ObjectsExtensions
    {
        public static IDictionary<string, T> UtilToDictionary<T>(this object source)
        {
            if (source == null) throw new NullReferenceException("Unable to convert anonymous object to a dictionary. The source anonymous object is null.");

            var dictionary = new Dictionary<string, T>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            {
                object value = property.GetValue(source);
                if (IsOfType<T>(value) || value == null)
                {
                    dictionary.Add(property.Name, (T)value);
                }
            }
            return dictionary;
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }
    }
}
