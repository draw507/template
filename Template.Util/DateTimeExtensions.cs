﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template.Util
{
    public static class DateTimeExtensions
    {
        public static string ToDateFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static string ToDateFormat(this DateTime? date)
        {
            return date == null ? string.Empty : date.Value.ToString("yyyy-MM-dd");
        }
    }
}
