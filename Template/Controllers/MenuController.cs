﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Template.Models;

namespace Template.Controllers
{
//MenuController - cambiando
    public class MenuController : Controller
    {
        [ChildActionOnly]
        public ActionResult Index()
        {
        //Lista de opciones
            List<MenuViewModel> menu = new List<MenuViewModel>
            {
                new MenuViewModel
                {
                    id = "1",
                    action = "Home/Index",
                    optionName = "Menu 1",
                    childOf = "",
                    icon = "dashboard",
                    order = 2,
                    selected = false
                },
                new MenuViewModel
                {
                    id = "2",
                    action = "",
                    optionName = "Menu 2",
                    childOf = "",
                    icon = "build",
                    order = 1,
                    selected = false
                },
                new MenuViewModel
                {
                    id = "3",
                    action = "",
                    optionName = "Menu 3",
                    childOf = "",
                    icon = "commute",
                    order = 3,
                    selected = false
                },
                new MenuViewModel
                {
                    id = "4",
                    action = "",
                    optionName = "Menu item 1",
                    childOf = "3",
                    icon = "face",
                    order = 2,
                    selected = false
                },
                new MenuViewModel
                {
                    id = "4",
                    action = "",
                    optionName = "Menu item 2",
                    childOf = "3",
                    icon = "face",
                    order = 1,
                    selected = false
                },
                new MenuViewModel
                {
                    id = "5",
                    action = "",
                    optionName = "Menu 4",
                    childOf = "",
                    icon = "flight_takeoff",
                    order = 5,
                    selected = false
                },
            };

            #region ITEM SELECTED

            string action = string.Format("{0}/{1}", 
                ControllerContext.ParentActionViewContext.RouteData.Values["controller"], 
                ControllerContext.ParentActionViewContext.RouteData.Values["action"]);

            menu.Where(a => a.action.ToUpper() == action.ToUpper()).Select(s => { s.selected = true; return s; }).ToList();

            #endregion

            return View("_SideBar", menu);
        }
    }
}