﻿var app = (function (window, undefined) {
    var st = {
        //cssNavbarToggler: ".navbar-toggler"
    };
    var dom = {}
    var catchDom = function () {
        dom.Document = $(document);
    };
    var suscribeEvents = function () {
        //dom.Document.on("click", st.cssNavbarToggler, events.eToggled);
    };
    var events = {
        eEvents: function (e) {
        }
    };
    var MostrarSpinner = function () {
        if (!$("#spinner").is(":visible")) {
            $("#spinner").modal();
        }
    };
    var OcultarSpinner = function () {
        $("#spinner").modal("hide");
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    };
    var MostrarMensajeModal = function (titulo, contenido) {
        $("#modal-title").html(titulo);
        $("#modal-body").html(contenido);
        $("#modal-footer").html('<button id="btn-close-modal" type="button" class="btn btn-default" data-dismiss="modal" data-action="">Cerrar</button>');
        $("#modal").modal();
    };
    var Notify = function (title, text) {
        new PNotify({
            title: title,
            text: text,
            type: 'success',
            cornerclass: "",
            animate_speed: 600,
            delay: 3000,
            styling: "bootstrap3",
            stack: stack_bar_top,
            animate: {
                animate: true,
                in_class: "fadeInRight",
                out_class: "fadeOutRight"
            }
        });
    };
    var iniciarDatetimePicker = function () {
        $(".datepicker").bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            lang: 'es',
            okText: 'Seleccionar',
            cancelText: 'Cerrar',
            nowButton: true,
            nowText: 'Hoy',
            weekStart: 0,
            time: false
        });
    };
    var initComponents = function () {
    };
    var initialize = function () {
        catchDom();
        suscribeEvents();
        initComponents();
    };
    return {
        init: initialize
    }
})(window);

$(function () {
    app.init();
});