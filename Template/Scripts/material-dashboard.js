var InitApp = (function (window, undefined) {
    var transparent = true;
    var mobile_menu_visible = 0, mobile_menu_initialized = false;
    var constantes = {
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ning�n dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "�ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    };
    var stack_topleft = { "dir1": "down", "dir2": "right", "push": "top" };
    var stack_bottomleft = { "dir1": "right", "dir2": "up", "push": "top" };
    var stack_custom = { "dir1": "right", "dir2": "down" };
    var stack_custom2 = { "dir1": "left", "dir2": "up", "push": "top" };
    var stack_modal = { "dir1": "down", "dir2": "right", "push": "top", "modal": true, "overlay_close": true };
    var stack_bar_top = { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 };
    var stack_bar_bottom = { "dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0 };
    var st = {
        cssNavbarToggler: ".navbar-toggler"
    };
    var dom = {}
    var catchDom = function () {
        dom.Document = $(document);
    };
    var suscribeEvents = function () {
        dom.Document.on("click", st.cssNavbarToggler, events.eToggled);
    };
    
    //Activate collapse right menu when the windows is resized
    $(window).resize(function () {
        md.initSidebarsCheck();
    });
    $('#modal').on('hidden.bs.modal', function (e) {
        $('.modal-backdrop').remove();
    })
    var events = {
        eToggled: function (e) {
            $toggle = $(this);

            if (mobile_menu_visible == 1) {
                $('html').removeClass('nav-open');

                $('.close-layer').remove();
                setTimeout(function () {
                    $toggle.removeClass('toggled');
                }, 400);

                mobile_menu_visible = 0;
            } else {
                setTimeout(function () {
                    $toggle.addClass('toggled');
                }, 430);

                var $layer = $('<div class="close-layer"></div>');

                if ($('body').find('.main-panel').length != 0) {
                    $layer.appendTo(".main-panel");

                } else if (($('body').hasClass('off-canvas-sidebar'))) {
                    $layer.appendTo(".wrapper-full-page");
                }

                setTimeout(function () {
                    $layer.addClass('visible');
                }, 100);

                $layer.click(function () {
                    $('html').removeClass('nav-open');
                    mobile_menu_visible = 0;

                    $layer.removeClass('visible');

                    setTimeout(function () {
                        $layer.remove();
                        $toggle.removeClass('toggled');

                    }, 400);
                });
                $('html').addClass('nav-open');
                mobile_menu_visible = 1;
            }
        }
    };    
    var debounce = function (func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            }, wait);
            if (immediate && !timeout) func.apply(context, args);
        };
    };
    var md = {
        misc: {
            navbar_menu_visible: 0,
            active_collapse: true,
            disabled_collapse_init: 0,
        },
        checkSidebarImage: function () {
            $sidebar = $('.sidebar');
            image_src = $sidebar.data('image');
            if (image_src !== undefined) {
                sidebar_container = '<div class="sidebar-background" style="background-image: url(' + image_src + ') "/>';
                $sidebar.append(sidebar_container);
            }
        },
        initSliders: function () {
            // Sliders for demo purpose
            var slider = document.getElementById('sliderRegular');
            noUiSlider.create(slider, {
                start: 40,
                connect: [true, false],
                range: {
                    min: 0,
                    max: 100
                }
            });
            var slider2 = document.getElementById('sliderDouble');
            noUiSlider.create(slider2, {
                start: [20, 60],
                connect: true,
                range: {
                    min: 0,
                    max: 100
                }
            });
        },
        initSidebarsCheck: function () {
            if ($(window).width() <= 991) {
                if ($sidebar.length != 0) {
                    md.initRightMenu();
                }
            }
        },
        initMinimizeSidebar: function () {
            $('#minimizeSidebar').click(function () {
                var $btn = $(this);
                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;
                } else {
                    $('body').addClass('sidebar-mini');
                    md.misc.sidebar_mini_active = true;
                }
                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);
                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });
        },
        checkScrollForTransparentNavbar: debounce(function () {
            if ($(document).scrollTop() > 260) {
                if (transparent) {
                    transparent = false;
                    $('.navbar-color-on-scroll').removeClass('navbar-transparent');
                }
            } else {
                if (!transparent) {
                    transparent = true;
                    $('.navbar-color-on-scroll').addClass('navbar-transparent');
                }
            }
        }, 17),
        initRightMenu: debounce(function () {
            $sidebar_wrapper = $('.sidebar-wrapper');
            if (!mobile_menu_initialized) {
                $navbar = $('nav').find('.navbar-collapse').children('.navbar-nav');
                mobile_menu_content = '';
                nav_content = $navbar.html();
                nav_content = '<ul class="nav navbar-nav nav-mobile-menu">' + nav_content + '</ul>';
                //navbar_form = $('nav').find('.navbar-form').get(0).outerHTML;
                $sidebar_nav = $sidebar_wrapper.find(' > .nav');
                // insert the navbar form before the sidebar list
                $nav_content = $(nav_content);
                //$navbar_form = $(navbar_form);
                $nav_content.insertBefore($sidebar_nav);
                //$navbar_form.insertBefore($nav_content);
                $(".sidebar-wrapper .dropdown .dropdown-menu > li > a").click(function (event) {
                    event.stopPropagation();

                });
                // simulate resize so all the charts/maps will be redrawn
                window.dispatchEvent(new Event('resize'));
                mobile_menu_initialized = true;
            } else {
                if ($(window).width() > 991) {
                    // reset all the additions that we made for the sidebar wrapper only if the screen is bigger than 991px
                    //$sidebar_wrapper.find('.navbar-form').remove();
                    $sidebar_wrapper.find('.nav-mobile-menu').remove();

                    mobile_menu_initialized = false;
                }
            }
        }, 200),
    }
    var initComponents = function () {
        var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows) {
            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
            $('html').addClass('perfect-scrollbar-on');
        } else {
            $('html').addClass('perfect-scrollbar-off');
        }
        $('body').bootstrapMaterialDesign();

        $sidebar = $('.sidebar');

        md.initSidebarsCheck();
        md.initMinimizeSidebar();

        window_width = $(window).width();

        // check if there is an image set for the sidebar's background
        md.checkSidebarImage();

        //    Activate bootstrap-select
        if ($(".selectpicker").length != 0) {
            $(".selectpicker").selectpicker();
        }

        //  Activate the tooltips
        $('[data-toggle="tooltip"]').tooltip();

        $('.form-control').on("focus", function () {
            $(this).parent('.input-group').addClass("input-group-focus");
        }).on("blur", function () {
            $(this).parent(".input-group").removeClass("input-group-focus");
        });
        // remove class has-error for checkbox validation
        $('input[type="checkbox"][required="true"], input[type="radio"][required="true"]').on('click', function () {
            if ($(this).hasClass('error')) {
                $(this).closest('div').removeClass('has-error');
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
    };
    var consoleWarning = function () {
        console.clear();
        console.log('%cAdvertencia', 'color:red;font-weight: bold;font-size:xx-large;');
        console.log('%cLa consola del navegador es para uso de desarrolladores. Si intentas "hackear" este sitio web podr� deshabilitar tus credenciales de acceso.', 'color:black;font-size:large;');
    };
    var initialize = function () {
        catchDom();
        suscribeEvents();
        initComponents();
        consoleWarning();
    };
    return {
        init: initialize
    }
})(window);

$(function () {
    InitApp.init();
});