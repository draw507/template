﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Template.Models
{
    public class MenuViewModel
    {
        public string id { get; set; }
        public string action { get; set; }
        public string optionName { get; set; }
        public string childOf { get; set; }
        public string icon { get; set; }
        public int order { get; set; }
        public bool selected { get; set; }
    }
}