﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Template.Controllers;

namespace Template.Filters
{
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            try
            {
                var guid = Guid.NewGuid().ToString();
                
                if (filterContext.ExceptionHandled)
                {
                    return;
                }

                //Logger
                var ex = filterContext.Exception;
                ex.Data["GUID"] = guid;
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                string message = ValidateMessageError(filterContext.Exception, guid);

                filterContext.Controller.ViewBag.ErrorMessage = message;

                //AJAX Request
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var jsonResult = new ErrorController { ControllerContext = filterContext }.JsonErrorResult(message);
                    jsonResult.ExecuteResult(filterContext);

                    filterContext.ExceptionHandled = true;
                }
                else
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "~/Views/Shared/Error.cshtml",
                        ViewData = filterContext.Controller.ViewData
                    };
                    filterContext.ExceptionHandled = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Shared/Error.cshtml",
                    ViewData = filterContext.Controller.ViewData
                };
                filterContext.ExceptionHandled = true;
            }

            base.OnException(filterContext);
        }

        public string ValidateMessageError(Exception ex, string guid)
        {
            //string message = string.Format("{0} {1}", RecursosLocalizables.StringResources.ErrorInesperado, guid);

            //if (ex is NIIFException)
            //{
            //    message = ex.Message + " " + RecursosLocalizables.StringResources.ErrorInesperado + guid;
            //}

            return "";
        }
    }
}